    const express=require('express');
    const mongoose=require('mongoose');
    const dotenv=require('dotenv');
    dotenv.config()
    const app=express();
    const PORT=4007;

    // middleware
    app.use(express.json());
    app.use(express.urlencoded({extended:true}));

    // mongoose connection
    mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

    // db connection notification
    const db=mongoose.connection
    db.on("error", console.error.bind(console, 'connection error:'))
    db.once("open", ()=>console.log(`Connected to Database`))

    const userSchema= new mongoose.Schema(
        {
            userName: {
                type: String,
                required: [true,`Name is required`]
            },
            password: {
                type: String,
                required: [true,`Password is required`]
            }
        }
    )

    const User=mongoose.model(`User`, userSchema)

    app.post("/sign-up", (req, res)=>{
        // console.log(req.body)

        User.create({userName: req.body.userName,
            password: req.body.password
        }).then((result)=>{res.send(result)})
    })

















    app.listen(PORT, ()=> console.log(`Server connected to port ${PORT}`))